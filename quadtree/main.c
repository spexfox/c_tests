#include <stdio.h>

#define TRUE = 1
#define FALSE = 0

// Quad Tree example
typedef struct 
{
    int x;
    int y;
} Point;

typedef struct
{
    Point pos;
    int data;
} Node;

typedef struct
{
    Point topLeft;
    Point botRight;

    Node *n;

    Quad *topLeftTree;
    Quad *topRightTree;
    Quad *botLeftTree;
    Quad *botRightTree;

    void (*insert_);
    Node* (*search_);
    char (*inBoundary_);
} Quad;

void Quad_insert(Node *node)
{

}

Node* Quad_search(Point p)
{

}

char Quad_inBoundary(Point p)
{

}

int main()
{
    return 0;
}